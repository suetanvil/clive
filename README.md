# CLIVE - The Lisp from Abuse

This is the Lisp interpreter used by the game _Abuse_ (1995, Crack dot
Com) extracted into a standalone interpreter and static library.

_Abuse_ was released into the public domain in 1998 after Crack dot
Com's demise and has been maintained as an open-source project since.
This code came from the repository at https://github.com/Xenoveritas/abuse .

This project is here mostly as a curiousity.  However, the code is
open-source so you are free to do whatever you want with it.  See the
file `NOTES.md` for miscellaneous notes on the internals.

## Building

Building CLIVE requires a C++ compiler and `make`.  I used Clang++ on
Linux.  The process is primitive but simple.

It should be a matter of typing

    cd src
    make

but you may need to edit the `Makefile` to fix things.

## Running

The main executable is called `clive`.  Running it with no arguments
will drop you into a REPL:

> $ ./src/clive 
> Lisp: 110 symbols defined, 99 system functions, 3 pre-compiled functions
> 
> CLIVE - Lisp extracted from Crack.com's game 'Abuse'
> Open-Source software with NO WARRANTY; see included file LICENSE.
> ---
> Historical banner:
> 
> CLIVE (C) 1995 Jonathan Clark, all rights reserved
>    (C LISP interpreter and various extentions)
> Type (Ctrl-D) to exit
> Lisp> (+ 2 3)
> 5
> Lisp> (defun add3 (n) (+ n 3))
> add3
> Lisp> (add3 42)
> 45
> Lisp> End of input : bye

If run with a single argument, `clive` will attempt to load and run
that program:

> $ cat examples/hello_world.lsp
> 
> ;; Simple hello-world with a function
> 
> (defun hi (thing)
>   (print (concatenate 'string "Hello " thing "!")))
> 
> (hi "World")
> 
> $ ./src/clive examples/hello_world.lsp
> "Hello World!"

