
;; Simple hello-world with a function

(defun hi (thing)
  (print (concatenate 'string "Hello " thing "!")))

(hi "World")

