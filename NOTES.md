# Various Hacking Notes

This is what I've learned in the course of doing this.  Some of this
information may be useful to you as well.

## Stuff I've learned

1. The C++ code does not seem to take advantage of many of the
features of C++ even by 1995 standards.  It looks to me like either
the developers were C programmers still new to C++ or they were
limited by available compilers.

2. All of the built-in Lisp functions exist as `case` statements in
one of three functions rather than (e.g.) pointers to C++ functions.
Each built-in function has a number associated with it; the functions
`add_*_function` assocate the name with the number and that number
needs to match the `case` statement for the code.

3. Other external C functions used to interact with the greater game
are implemented the same way.  In this case, they are defined as case
statements in one of two functions: `c_caller` or `l_caller`.  The
former returns a C `long` while the later always returns a Lisp
object.  Barebones versions of both of these are in `clisp_stub.cpp`.

4. Lisp data types are all implemented as C++ objects and all derived from
class `LObject`.  It provides a poor-person's RTTI by putting a type
identifier in `LObject` field `m_type`.

5. Symbols are implemented by class `LSymbol`. Each instance holds
fields for a name, function and value; it appears that symbol objects
hold their corresponding values directly rather than being used as a
dictionary key.

6. Arithmetic is either integer or fixed-point math implemented with
integers.  Trigonometry operations are impemented using a table of
integer constants.

7. There is an interface for moving around opaque C++ objects via an
integer handle.  Class `LObjectVar` holds the object and
(stubbed) functions `l_obj_set`, `l_obj_get` and `l_obj_print` are
used to access it.  The rest of this mechanism was deeply entangled
into the guts of the game and had to be left out but it may be a
useful starting point for a more portable version of this mechanism.

8. The source files `spec.cpp` and `spec.hpp` provide code to access
_Abuse_ asset archives.  These can include Lisp code, so it may be
useful as part of CLIVE.

9. There is no language documentation.  The best you can do is look at
method `LSysFunction::EvalFunction` to try to figure out what the code
does.  (I started to do this and ran out of steam; there's a little
bit in `src/sys_funcs.inc`.)



## Things I've changed

1. The new source file `main.cpp` provides an entry point for a
standalone interpreter (`clive`).  In addition, the remaining sources
are compiled into a static library.

2. All header files now have the suffix `.hpp` instead of `.h`.

3. `make` is now used for building instead of `cmake`.  In addition,
`config.h` has been dropped in favour of putting the relevant
`#define`s in the compiler command line.  (Justification: this program
is small enough that using cmake is more trouble than its worth.)

4. A few things now use STL types.  This is the thin edge of the
wedge.  Or not.

5. The "getter" hook in `dprint.[ch]pp` (i.e. the function that gets
called to read text from the user) now returns `true` on success and
`false` on EOF or error.  This lets us handle CTRL+D a bit more
gracefully.

6. In the spirit of DRY, the array of strings holding the names of
system functions and its corresponding enum were combined into file
`sys_funcs.inc` and used to generate both via repeated `#include`
directives and redefining macros.  (I also started writing function
docs there but ran out of steam after the first one.  Maybe later.)



## Outstanding Bugs and Quirks

1. There is no single function which will take a C string and evaluate
it as Lisp code.  Instead, there are several slightly different
sections of code in other functions.  These should be unified into a
single function.

2. Related to 1, function `end_of_program()` in `lisp.cpp` uses a
fixed-size static buffer with no bounds checking; this should really
be working with `std::string` objects.

3. When parsing numbers, trailing non-digit non-whitespace characters
after a number are ignored.  That is, `123abc` is parsed as `123`.

4. Errors in expressions evaluated in the REPL drop into the debugger
on error and then exit the interpreter.  This should really be
handled more gracefully.



## Stuff I'd fix if I had the Energy and/or Motivation

1. Better type safety in the code.  There is a lot of stuff being
passed around by `void` pointers that are then cast to the expected
type.  These days, C++ is very good at letting you keep type
information at compile time and that makes it much it easier for the
compiler to detect subtle errors.

2. Use more modern C++ stuff in general.  There are many places that
could benefit from the things modern C++ has to offer.

3. Change how binding to C++ code works from putting it in a `case`
clause to using lambdas and/or function pointer callbacks.

4. Adding some regression tests and documentation.
