
#include <lisp.hpp>
#include <lisp_gc.hpp>

#include <stdio.h>
#include <unistd.h>


//
// Hooks to external subs
//

// call by lisp_init, defines symbols and functions to interface with c
void clisp_init()
{

    // Random number
    add_c_function("random",1,1,                  9);

    add_lisp_function("get_cwd",0,0,              54);
    add_lisp_function("system",1,1,               55);
}


// Invoke C code and return a Lisp object.
// 
// Note : args for l_caller have not been evaluated yet!
void *l_caller(long number, void *args)
{
  PtrRef r1(args);
  switch (number)
  {
    case 54 :
    {
      char buf[1024];
      const char *cd = getcwd(buf, sizeof(buf));
      if (!cd) {
          cd = "";
      }

      return LString::Create(cd);
    } break;
    
    case 55 :
      system(lstring_value(CAR(args)->Eval()));
      break;
  }
  return NULL;
}
        


// Invoke C code and return a long.
// 
// arguments have already been evaled..
long c_caller(long number, void *args)
{
    PtrRef r1(args);
    switch (number)
    {
    case 9:
        // This used to look up a number from a table of random
        // numbers, but that code's been left behind.
        return rand() % 0xFFFF;
        break;

    default :
        printf("Undefined c function %ld\n",number);
        return 0;
    }
    return 0;
}
