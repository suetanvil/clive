
#include <lisp.hpp>
#include <lisp_gc.hpp>
#include <dprint.hpp>

#include <string.h>
#include <stdio.h>

#include <string>
#include <vector>



static char *get_line(int open_braces)
{
    char *line=(char *)malloc(1000);
    fgets(line, 1000, stdin);
    
    char prev=' ';
    for(char *s = line; *s && (prev!=' ' || *s!=';'); s++)
    {
        prev=*s;
        if(*s=='(') open_braces++;
        else if(*s==')') open_braces--;
    }
    
    if(open_braces < 0)
        fprintf(stderr, "\nToo many)'s\n");
    else if(open_braces > 0)
    {
        char *s2 = get_line(open_braces);
        line=(char *)realloc(line, strlen(line)+strlen(s2)+1);
        strcat(line, s2);
        free(s2);
    }
    return line;
}

static void repl() {
    char const *eof_char = "Ctrl-D";
    printf(
        "\n"
        "CLIVE - Lisp extracted from Crack.com's game 'Abuse'\n"
        "Open-Source software with NO WARRANTY; see included file LICENSE.\n"
        "---\n"
        "Historical banner:\n\n"
        "CLIVE (C) 1995 Jonathan Clark, all rights reserved\n"
        "   (C LISP interpreter and various extentions)\n"
        "Type (%s) to exit\n", eof_char);
    
    while(!feof(stdin))
    {
        fprintf(stderr, "Lisp> ");
        char *l = get_line(0);
        char const *s = l;
        while(*s)
        {
            LObject *prog = LObject::Compile(s);
            l_user_stack.push(prog);
            while(*s==' ' || *s=='\t' || *s=='\r' || *s=='\n') s++;
            prog->Eval()->Print();
            l_user_stack.pop(1);
        }
        free(l);
    }
    printf("End of input : bye\n");
}


static void game_printer(char *st)
{
    fprintf(stdout, "%s", st);
}

static bool game_getter(char *dest, int buffer_size) {
    if (buffer_size <= 0) { return false; }

    dest[0] = 0;
    char* success = fgets(dest, buffer_size, stdin);
    if(!success) { return false; }

    // Wipe out the trailing newline.  (TODO: deal with DOS-style
    // CR+LF sequences.)
    size_t len = strlen(dest);

    while(len > 0) {
        char last = dest[len - 1];
        if (last != '\n' && last != '\r') { break; }
        dest[len - 1] = 0;
        --len;
    }

    return true;
}

static void go(const std::vector<char*>& sources)
{
    bool replmode = sources.size() == 0;

    set_dgetter(game_getter);
    
    // Lisp::Init() prints some messages; we want them if and only if
    // we're in a repl so this determines when we set up the printer
    // callback.
    if (replmode) { set_dprinter(game_printer); }
    Lisp::Init();
    if (!replmode) { set_dprinter(game_printer); }

    
    // If there are no arguments, drop into the repl
    if (replmode) {
        repl();
        return;
    }

    for (const char* src : sources) {
        // This is hacky, but it seems to be the standard way to load
        // a file in Abuse:
        std::string cmd = "(load \"";
        cmd += src;
        cmd += "\")";
        
        LObject *prog = LObject::Compile(cmd);
        LObject *result = prog->Eval();

        if (!result) { exit(1); }
    }
}

int main(int argc, char *argv[])
{
    std::vector<char*> sources;
    for (int n = 1; n < argc; n++)
    {
        sources.push_back(argv[n]);
    }
    
    go(sources);
    
    return 0;
}
